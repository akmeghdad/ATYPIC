document.addEventListener('DOMContentLoaded', function () {

    document.getElementById('js_comparison').addEventListener('click', function(){
        var stringA = document.getElementById('js_txtA').value;
        var stringB = document.getElementById('js_txtB').value;
        var h1 = document.getElementById('js_heading_1');

        h1.className = '';

        if (isAnagram(stringA, stringB)) {
            h1.textContent ='These strings are Anagram :)';
            h1.classList.add('positive');

        } else {
            h1.textContent ='Sorry! These strings are not Anagram :(';
            h1.classList.add('negative');
        }
    })

});

function isAnagram(txtA, txtB) {
    var resultA = sortText(txtA);
    var resultB = sortText(txtB);

    if (resultA == resultB ) {
        return true;
    }
    return false;
}

function sortText(txt) {
    txt = txt.toLowerCase().replace(/ /g,'')
    return txt.split('').sort().join('');
}