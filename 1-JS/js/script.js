document.addEventListener('DOMContentLoaded', function () {
    
    document.getElementById('js_submit').addEventListener('click', function(){
        var text = document.getElementById('js_txt').value;
        var h1 = document.getElementById('js_heading_1');

        h1.className = '';

        if (isPalindrome(text)) {
            h1.textContent ='This string is Palindrome :)';
            h1.classList.add('positive');

        } else {
            h1.textContent ='Sorry! This string is not Palindrome :(';
            h1.classList.add('negative');
        }
    });
});

function isPalindrome(txt) {
    var str = getPalindrome(txt);

    if (str == txt.toLowerCase()) {
        return true;
    }
    return false;
}

function getPalindrome(txt) {
    txt = txt.toLowerCase()
    return txt.split('').reverse().join('');
}