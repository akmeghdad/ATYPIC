<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>A T Y P I C | Blog</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="view/css/normalize.css">
    <link rel="stylesheet" href="view/css/base.css">
    <link rel="stylesheet" href="view/css/style.css">
</head>
<body>
    <header>
        <nav class="container">
            <h1 class="menu__heading_1">My Blog</h1>
            <ul class="menu__list">
                <li class="menu__list-item"><a href="#">Home</a></li>
                <li class="menu__list-item"><a href="#">Archive</a></li>
                <li class="menu__list-item"><a href="#">About us</a></li>
                <li class="menu__list-item"><a href="#">Contact US</a></li>
            </ul>
        </nav>
    </header>
    <main class="container">
        <section class="post">
            <?php foreach ($dbPost as $value): ?>
                <article class="post__article">
                    <h2 class="post__heading_2"><?=$value["title"]?></h2>
                    <div class="post__info">
                        <div class="post__author"><i class="fas fa-user-edit"></i> <?=$value["author"]["name"]?></div>
                        <div class="post__date"><i class="far fa-calendar-alt"></i> <?=$value["date"]?></div>
                    </div>
                    <div class="post__contents"><?=$value["content"]?></div>
                    <div class="tags post__tags">
                        <ul class="tags__list">
                            <i class="fas fa-tags"></i>
                            <p class="tags__paragraph">Tags: </p>    
                            <?php foreach ($value["tags"] as $valueTag): ?>
                                <li class="tags__list-item"><?=$valueTag?></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </article>
            <?php endforeach;?>

        </section>
        <aside>
        <section class="sort">
            <h3 class="sort__heading_3">Sort By</h3>
            <ul class="sort__list">
                <li class="sort__list-item"><a href="index.php?sort=date">Date</a></li>
                <li class="sort__list-item"><a href="#">Id of post</a></li>
                <li class="sort__list-item"><a href="#">Alphabet</a></li>
            </ul>
        </section>
        <section class="list-author">
            <h3 class="list-author__heading_3">Our Authors</h3>
            <ul class="list-author__list">
                <li class="list-author__list-item"><a href="#">Date</a></li>
                <li class="list-author__list-item"><a href="#">Id of post</a></li>
                <li class="list-author__list-item"><a href="#">Alphabet</a></li>
            </ul>
        </section>

        </aside>
    </main>
    <footer>
        <p class="footer__paragraph">This is a Footer</p>
    </footer>
</body>
</html>